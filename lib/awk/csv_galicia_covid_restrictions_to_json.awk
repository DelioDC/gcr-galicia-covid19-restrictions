#!/usr/bin/gawk
################################################################################
# Version: 20210714
# Author: deliodc (https://gitlab.com/DelioDC)
################################################################################
# License:
################################################################################
# Copyright 2020 Delio Docampo Cordeiro
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################
# Records:
# RS  - Record Separator
# ORS - Output Record Separator
# NR  - total Number of Records or Line Number
# FNR - Number of Records relative to the current input file
# ------------------------------------------------------------------------------
# Fields:
# FS  - Field Separator
# OFS - Output Field Separator
# NF  - Number of Fields in a record
# ------------------------------------------------------------------------------
# FILENAME - Name of input file.
################################################################################
# record_1(field1,field2,field3)
# record_2(field1,field2,field3)
# record_3(field1,field2,field3)
################################################################################
# https://www.gnu.org/software/gawk/manual/html_node/index.html#SEC_Contents
################################################################################
function clean_surrounding_quotes (string) {
    # Returns the string with out start and end '\\"'
    return gensub(/"(.*)"/, "\\1", "g", string);
}
function to_capitalizate (string) {
    # Returns string with first character uppercase and rest lowercase
    return toupper(substr(string,1,1)) tolower(substr(string,2));
}

function br_to_newline (string) {
    # Returns the string with the "<br>" or "<br/>" replaced by "\n- ":
    string = gensub(/(<br>|<br\/>)/, "\\\\n", "g", string);
    # Needed to replace "\n \n" substrings:
    string = gensub( "\\\\n\\\\n", "\\\\n" , "g", string);
    # Adds a dot at the end of lines, a dash and 1 space at the start of lines:
    string = gensub( "\\\\n", ".\\\\n- " , "g", string);
    # Replace multiple dots
    string = gensub( "\\.\\." , ".", "g", string);
    # Remove posible last empty line:
    string = gensub( "\\\\n- $" , "", "g", string);
    string = gensub(/\\n- \./, "", "g", string);
    # Add the first dash:
    string = "- "string;
    return string;
}

function remove_lines_with_a_entities (string) {
    # Remove lines like:
    # \n- <a href='https://coronavirus.sergas.gal/Contidos/Restricions-Nivel-Medio-Alto'>[+info]</a>
    string = gensub(/<a[ '/>\[\]\-a-záéíóúñA-ZÁÉÍÓÚÑ0-9@:%._\+~#=\[\]]*<\/a>/, "", "g", string);
    return string;
}
function remove_empty_lines(string){
    string = gensub(/-  - /, "" , "g", string);
    return string;
}
function clean_description (string) {
    string = remove_lines_with_a_entities(string);
    string = br_to_newline(string);
    string = remove_empty_lines(string);
    return string;
}
BEGIN{
    FS=",";
    EXPECTED_RECODS=313;
    printf "{\"town\":[\n";
}
{
    if (NR == 1) {
        next; # Omit the first record
    }

    for (i=1; i<=NF; i++) {
        # Set id_raw
        if ( $i ~ /[0-9]{11,11}/ ) {
            id_raw=$i;
        }
        # Set name_raw
        if ( $i ~ /^[áéíóúñÑa-zA-Z ]{1,33}$/ ) {
            name_raw=$i;
        }
        # Set description_raw
        if ( $i ~ /[<>:]?[a-z]{38}/g ) {
            description_raw=$i;
        }
        # Set level_raw
        if ( $i ~ /^[0-9]{1,2}$/ ) {
            level_raw=$i;
        }
    }

    #ID-Detalle-Nome-Restricion
    id = id_raw;
    name = to_capitalizate(name_raw);
    if(id != description_raw){
        description = clean_description(description_raw);
    }else{
        description = "Sen resumo, ver enlace adxunto.";
    }

    level = level_raw;

    # Print the JSON object:
    printf "\t{\n";
    printf "\t\t\"id\":\"%s\",\n", id;
    printf "\t\t\"name\":\"%s\",\n", name;
    printf "\t\t\"description\":\"%s\",\n", description;
    printf "\t\t\"level\":\"%s\"\n", level;
    printf "\t}";
    if ( (NR - 1) < EXPECTED_RECODS ) {
        printf ",";
    }
    printf "\n";

}
END{
    printf "]}\n";
}

#!/usr/bin/bash
################################################################################
# Version: 20220226
# Author: deliodc (https://gitlab.com/DelioDC)
# ! This is a unofficial script, use only for test purposes.
################################################################################
# License:
################################################################################
# Copyright 2020 Delio Docampo Cordeiro
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################
# Colors for echo -e:
E_ERROR='\e[1;31m'   # Bold, red
E_INFO='\e[0;34m'    # Blue
E_WORK='\e[0;32m'    # Green
E_NC='\033[0m'       # No color
SCRIPT_LIB_BASE_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
function echo_error_message {
    ####################################
    # Description:
    #  Prints a error message in &2 with color of $E_ERROR
    ####################################
    # Params:
    #  - Error message
    ####################################
    >&2 echo -e  $E_ERROR"ERROR: $1"$E_NC
}

function exit_with_error_message {
    ####################################
    # Description:
    #  Prints error message and exits with defined return code
    ####################################
    if [[ ! -z $1 ]]; then
        echo_error_message "$1"
        exit
    else
        echo_error_message "function exit_with_error_message needs 1 parameter"
        exit
    fi
}

function curl_call (){
    ####################################
    # Description:
    #  Returns the web response.
    ####################################
    # Parameters:
    #  - web url
    ####################################
    if [[ ! -z $USER_AGENT && ! -z $1 ]]; then
        curl  -s --user-agent "$USER_AGENT" "$1"
    else
        exit_with_error_message "curl_call parameters not defined in curl_call function call"
    fi
}

function get_redirection_url () {
    ####################################
    # Description:
    #  Parses the url redirection response.
    ####################################
    if [[ ! -z "$USER_AGENT" && ! -z "$1" ]]; then
        curl -s --user-agent "$USER_AGENT" -L "$1" | grep -Eo "(http|https):[^\"\']+" | uniq
    fi
}

function parse_csv_with_awk_script () {
    # Description:
    #  Parses CSV data with the awk provided script.
    #######################################################
    # Params:
    #  - CSV data.
    #  - Path to awk script file.
    #######################################################
    if [[ ! -z "$1" && ! -z "$2" ]]; then
        printf "%s"  "$1" | awk -f "$2"
    fi
}



# GCR - Galicia Covid19 Restrictions

Parses official info published in [coronavirus.sergas.gal](https://coronavirus.sergas.gal/Contidos/Restriccions-concellos),
and returns galician towns resctrictions by covid19.

_*This script is unofficial, and we don't provide any guarantees of the provided data is correct, only use for test purposes._

## Description

Requests a JSON with the galician towns restrictions by covid19, to the official Sergas website:
     https://coronavirus.sergas.gal/Contidos/Restriccions-concellos

Parses the CSV data embed in the requested JSON, proceses data and prints a new JSON with all towns data or one town data or a list of towns ids.

### Licensing

See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.

## Required dependences

| Name: | Tested with version: | Website:                                                                            |
| :---: | :---:                | :---                                                                                |
| curl  | 7.73.0               | [https://curl.haxx.se](https://curl.haxx.se)                                        |
| jq    | jq-1.6               | [https://github.com/stedolan/jq](https://github.com/stedolan/jq/)                   |
| gawk  | 5.1.0                | [https://www.gnu.org/software/gawk/](https://www.gnu.org/software/gawk/)            |
| grep  | 3.6                  | [https://www.gnu.org/software/grep/](https://www.gnu.org/software/grep/)            |
| sed   | 4.8                  | [https://www.gnu.org/software/sed/](https://www.gnu.org/software/sed/)              |
| cut   | 8.32                 | [https://www.gnu.org/software/coreutils/]([https://www.gnu.org/software/coreutils/) |
| date  | 8.32                 | [https://www.gnu.org/software/coreutils/]([https://www.gnu.org/software/coreutils/) |

## Usage

Usage:

```
./galicia-covid19-restrictions.sh [PARAMETER]
```

|Parameter:                                             |Description:                                                                |
|:---                                                   | :---                                                                       |
| -h, --help                                            | Prints the help.                                                           |
| -a, --all-towns-json                                  | Returns a json with all towns data.                                        |
| -i, --json-id-list                                    | Returns a json with all towns "name":"id" data list.                       |
| -A, --all-towns-csv                                   | Returns a csv with all towns data.                                         |
| --json-by-town-name="town_name_string"                | Returns a json with objects which name contains town_name_string string.   |
| --json-by-town-id="id_number(11 characteres)"         | Returns a json with data of the town selected by id.                       |
| --message-by-town-id="id_number(11 characteres)"      | Formats the town JSON information to human readable and prints the result. |

### Examples

#### Print a JSON with all towns data:

```
    ./galicia-covid19-restrictions.sh --all-towns-json
```

**RESULT:**

```
{
"publishedAt": "<datetime_in_utc>",
"official_source": "<url_to_official_source>",
"town": [
    {
    "id": "34121515001",
    "name": "Abegondo",
    "description": "<string_with_the_restrictions>",
    "level": "<number>"
    },
    {
    "id": "34121515002",
    "name": "Ames",
    "description": "<string_with_the_restrictions>",
    "level": "<number>"
    },
    ... rest of json objects ...
    {
    "id": "34123636902",
    "name": "Cerdedo-cotobade",
    "description": "<string_with_the_restrictions>",
    "level": "<number>"
    }
]
}
```

#### Print JSON with only all towns id and name:

```
./galicia-covid19-restrictions.sh -i
```

**RESULT:**

```
    [
        ... rest of json objects ...
        {
            "name": "Vigo",
            "id": "34123636057"
        },
        ... rest of json objects ...
    ]
```

#### Print JSON of town name who contains "vig":

```
./galicia-covid19-restrictions.sh --json-by-town-name="Vig"
```

**RESULT:**

```
    {
        "id": "34123636057",
        "name": "Vigo",
        "description": "<string_with_the_restrictions>",
        "level": "<number>"
        "publishedAt": "<datetime_in_utc>",
        "official_source": "<url_to_official_source>"
    }
```

#### Print JSON of town which id is 34123636057:

```
./galicia-covid19-restrictions.sh --json-by-town-id=34123636057
```

**RESULT:**

```
    {
        "id": "34123636057",
        "name": "Vigo",
        "description": "<string_with_the_restrictions>",
        "level": "<number>"
        "publishedAt": "<datetime_in_utc>",
        "official_source": "<url_to_official_source>"
    }
```


#### Print MESSAGE (in galician) of town which id is 34123636057:

```
./galicia-covid19-restrictions.sh --message-by-town-id=34123636057
```

**RESULT:**

```
🦠#COVID19 Vigo.
Nivel: 6 🔏 (Medidas de prevención específicas en Galiza)
----------------------------
Restriccións (2021-01-27 11:31):
- Peche das actividades non esenciais
- Agrupacións de persoas conviventes
- Restrinxida a mobilidade nocturna de 22:00 a 6:00
- Limitacións especiais de mobilidade
- Peche establecementos comerciais: 18:00 horas
- Peche centros e parques comerciais: 18:00 horas e os sábados e domingos
____________________________
fonte: https://coronavirus.sergas.gal/Contidos/Restriccions-concellos
+info: https://coronavirus.sergas.gal/Contidos/Medidas-prevencion-especificas-Galicia
```

